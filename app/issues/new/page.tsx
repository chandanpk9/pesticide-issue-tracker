"use client";
import "easymde/dist/easymde.min.css";
import IssueForm from "../__components/IssueForm";

const NewIssue = () => {
  return <IssueForm />;
};

// export const metadata: Metadata = {
//   title: "Create new Ticket",
//   description: "Summary of project issues",
// };

export default NewIssue;

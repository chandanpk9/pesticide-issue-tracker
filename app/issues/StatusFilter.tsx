import { Flex, Button } from "@radix-ui/themes";
import Link from "next/link";
import React from "react";
import IssueStatusFilter from "./IssueStatusFilter";

const StatusFilter = () => {
  return (
    <Flex justify="between" mb={"4"}>
      <IssueStatusFilter />
      <Button>
        <Link href={"/issues/new"}>Create New Issue</Link>
      </Button>
    </Flex>
  );
};

export default StatusFilter;

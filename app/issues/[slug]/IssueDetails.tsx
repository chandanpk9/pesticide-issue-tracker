import { IssueStatusBadge } from "@/app/Components";
import { Issue } from "@prisma/client";
import { Box, Heading, Flex, Text } from "@radix-ui/themes";
import React from "react";

interface Props {
  issue: Issue;
}

const IssueDetails = ({ issue }: Props) => {
  return (
    <Box className="space-y-3 col-span-3">
      <Heading size={"7"}>{issue.title}</Heading>
      <Flex gap={"3"}>
        <IssueStatusBadge status={issue.status} />
        <Text className="">{issue.created_at.toDateString()}</Text>
      </Flex>
      <Text>{issue.description}</Text>
    </Box>
  );
};

export default IssueDetails;

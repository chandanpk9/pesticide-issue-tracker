import prisma from "@/prisma/client";
import { Button, Grid, Box, Flex } from "@radix-ui/themes";
import { notFound } from "next/navigation";
// import EditButton from "./EditButton";
import IssueDetails from "./IssueDetails";
import { default as EditButton } from "@/app/Components/CustomNavButton";
import { Pencil2Icon, TrashIcon } from "@radix-ui/react-icons";
import { getServerSession } from "next-auth";
import { nextAuthOptions } from "@/app/auth/authOptions";
import AssigneeSelect from "./AssigneeSelect";

interface Props {
  params: {
    slug: string;
  };
}

const IssueDetailsPage = async ({ params }: Props) => {
  const session = await getServerSession(nextAuthOptions);

  const issue = await prisma.issue.findUnique({
    where: {
      id: parseInt(params.slug),
    },
  });

  if (!issue) notFound();

  return (
    <Grid columns={{ initial: "1", md: "6" }} className="m-4">
      <IssueDetails issue={issue} />
      <Box className="">
        {session && (
          <Flex direction={"column"} gap={"2"}>
            <AssigneeSelect issue={issue} />
            <EditButton
              path={`/issues/${issue.id}/edit`}
              text={"Edit Issue"}
              color="purple"
            >
              <Pencil2Icon />
            </EditButton>
            <Button color="red">
              <TrashIcon />
              Delete
            </Button>
          </Flex>
        )}
      </Box>
    </Grid>
  );
};

export async function generateMetadata({ params }: Props) {
  const issue = await prisma.issue.findUnique({
    where: {
      id: parseInt(params.slug),
    },
  });
  return {
    title: issue?.title,
    description: "Details of issue " + issue?.id,
  };
}
export default IssueDetailsPage;

import { Button } from "@radix-ui/themes";
import Link from "next/link";
import { ReactNode } from "react";

type Props = {
  color:
    | "tomato"
    | "red"
    | "ruby"
    | "crimson"
    | "pink"
    | "plum"
    | "purple"
    | "violet"
    | "iris"
    | "indigo"
    | "blue"
    | "cyan"
    | "teal"
    | "jade"
    | "green"
    | "grass"
    | "brown"
    | "orange"
    | "sky"
    | "mint"
    | "lime"
    | "yellow"
    | "amber"
    | "gold"
    | "bronze"
    | "gray";
  text: string;
  children?: ReactNode;
  path: string;
};

const CustomNavButton = ({ text, children, path, color }: Props) => {
  return (
    <>
      <Link style={{ display: "contents" }} href={path}>
        <Button color={color} className="hover:cursor-pointer">
          {children}
          {text}
        </Button>
      </Link>
    </>
  );
};

export default CustomNavButton;

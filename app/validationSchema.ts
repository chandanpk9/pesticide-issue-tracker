import { z } from "zod";

// Incoming data validation schema
export const issueSchema = z.object({
  title: z.string().min(2).max(255),
  description: z.string().min(10),
});

export const patchIssueSchema = z.object({
  title: z.string().min(2, "too smaal title").max(255).optional(),
  description: z.string().min(10, "required minimum of 10 chars").optional(),
  assignedUserId: z.string().min(1, "Invalid user ID").optional().nullable(),
});

import IssuesPage from "./IssuesPage";
import prisma from "@/prisma/client";
import { Issue, Status } from "@prisma/client";
import Pagination from "../Components/Pagination";
import { Metadata } from "next";

interface Props {
  searchParams: {
    status: "OPEN" | "IN_PROGRESS" | "CLOSED" | "ALL";
    page: string;
  };
}

const Issues = async ({ searchParams }: Props) => {
  const status =
    searchParams.status === "ALL" ? undefined : searchParams.status;

  const page = Number(searchParams.page) || 1;
  const pageSize = 10;

  const where = { status };
  const issues: Issue[] = await prisma.issue.findMany({
    where,
    skip: (page - 1) * pageSize,
    take: pageSize,
  });

  const itemCount = await prisma.issue.count({ where });

  return (
    <div className="ml-4 space-y-6 pr-3">
      <h2>Issues page</h2>
      <IssuesPage issues={issues} />
      <Pagination
        currentPage={page}
        itemCount={itemCount}
        pageSize={pageSize}
      />
    </div>
  );
};

export const revalidate = 0;

export const metadata: Metadata = {
  title: "Tickets view",
  description: "Listing all the raised tickets",
};

export default Issues;

import React from "react";
import { Table } from "@radix-ui/themes";
import { IssueStatusBadge } from "@/app/Components";
import Link from "@/app/Components/NextLink";
import StatusFilter from "./StatusFilter";
import { Issue, Status } from "@prisma/client";

interface Props {
  issues: Issue[];
}

const IssuesPage = async ({ issues }: Props) => {
  return (
    <div className="max-w-screen-lg">
      <StatusFilter />
      <Table.Root variant="surface" size={"2"}>
        <Table.Header>
          <Table.Row>
            <Table.ColumnHeaderCell>Issue</Table.ColumnHeaderCell>
            <Table.ColumnHeaderCell className="hidden md:table-cell">
              Status
            </Table.ColumnHeaderCell>
            <Table.ColumnHeaderCell className="hidden md:table-cell">
              Created
            </Table.ColumnHeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {issues.map((issue: Issue) => (
            <Table.Row key={issue.id}>
              <Table.RowHeaderCell>
                <Link href={`/issues/${issue.id}`}>{issue.title}</Link>
                <h6 className="block md:hidden">
                  Status: <IssueStatusBadge status={issue.status} />
                </h6>
                <h6 className="block md:hidden">
                  Created: {issue.created_at.toDateString()}
                </h6>
              </Table.RowHeaderCell>

              <Table.Cell className="hidden md:table-cell">
                <IssueStatusBadge status={issue.status} />
              </Table.Cell>
              <Table.Cell className="hidden md:table-cell">
                {issue.created_at.toDateString()}
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table.Root>
    </div>
  );
};

export default IssuesPage;

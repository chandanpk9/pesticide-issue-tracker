import { issueSchema, patchIssueSchema } from "@/app/validationSchema";
import { NextRequest, NextResponse } from "next/server";
import prisma from "@/prisma/client";
import { getServerSession } from "next-auth";
import { nextAuthOptions } from "@/app/auth/authOptions";

export async function PATCH(
  request: NextRequest,
  { params }: { params: { slug: string } }
) {
  // Protecting Api routes from unuthorised requests.
  const session = await getServerSession(nextAuthOptions);
  if (!session) {
    return NextResponse.json({ error: "access denied" }, { status: 401 });
  }

  // Extracing incomming body
  const body = await request.json();
  const isValid = patchIssueSchema.safeParse(body);
  if (!isValid.success) {
    console.log(isValid.error.errors, "Zod errorsss +++=====+++====");
    return NextResponse.json(
      { error: "api error Invalid data" },
      { status: 400 }
    );
  }
  // Check for Assignee update request
  const { assignedToUserId } = body;

  if (!!assignedToUserId) {
    try {
      const user = await prisma.user.findUnique({
        where: {
          id: assignedToUserId,
        },
      });
      if (!!!user) {
        return NextResponse.json({ error: "Invalid UserId" }, { status: 400 });
      }
    } catch (error) {
      console.log(error, "+++===");
    }
  }

  // Find and update issue
  const issue = await prisma.issue.findUnique({
    where: {
      id: parseInt(params.slug),
    },
  });

  if (!issue) {
    return NextResponse.json({ error: "Invalid Issue" }, { status: 400 });
  }

  // Updating issue
  const updatedIssue = await prisma.issue.update({
    where: {
      id: parseInt(params.slug),
    },
    data: {
      ...body,
    },
  });

  return NextResponse.json(updatedIssue, { status: 200 });
}

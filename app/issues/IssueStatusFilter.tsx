"use client";

import { Status } from "@prisma/client";
import { Select } from "@radix-ui/themes";
import { useRouter, useSearchParams } from "next/navigation";

const IssueStatusFilter = () => {
  const router = useRouter();
  const params = useSearchParams();
  const searchParam = params.get("status");

  const statuses: { label: String; state?: Status | null }[] = [
    { label: "ALL", state: null },
    { label: "Open", state: "OPEN" },
    { label: "In_Progress", state: "IN_PROGRESS" },
    { label: "Closed", state: "CLOSED" },
  ];

  return (
    <Select.Root
      onValueChange={(status) => {
        router.push(`/issues?status=${status}`);
      }}
      defaultValue={searchParam || ""}
    >
      <Select.Trigger placeholder="Filter by status.." />
      <Select.Content>
        <Select.Group>
          <Select.Label>Sugggestions</Select.Label>
          {statuses.map((status, i) => (
            <Select.Item key={i} value={status.state ? status.state : "ALL"}>
              {status.label}
            </Select.Item>
          ))}
          <Select.Separator />
        </Select.Group>
      </Select.Content>
    </Select.Root>
  );
};

export default IssueStatusFilter;

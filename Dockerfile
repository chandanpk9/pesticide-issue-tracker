FROM node:21-alpine

WORKDIR /usr/src/app

COPY ./package.json ./

RUN npm install

COPY . .

# RUN yarn build

EXPOSE 3000

# CMD [ "yarn", "start" ]
CMD [ "yarn", "dev" ]


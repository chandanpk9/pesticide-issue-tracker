import nextAuth from "next-auth";
import { nextAuthOptions } from "@/app/auth/authOptions";

const handler = nextAuth(nextAuthOptions);

export { handler as GET, handler as POST };

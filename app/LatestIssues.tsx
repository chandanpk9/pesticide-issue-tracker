import { Avatar, Card, Flex, Heading, Table, Text } from "@radix-ui/themes";
import prisma from "@/prisma/client";
import { IssueStatusBadge } from "./Components";
import Link from "./Components/NextLink";

const LatestIssues = async () => {
  const latestIssues = await prisma.issue.findMany({
    orderBy: {
      created_at: "desc",
    },
    take: 5,
    include: {
      assignedToUser: true,
    },
  });

  return (
    <Card>
      <Heading size={"7"} mb={"3"}>
        Latest Issues
      </Heading>
      <Table.Root>
        <Table.Body>
          {latestIssues.map((issue) => {
            return (
              <Table.Row key={issue.id}>
                <Table.Cell>
                  <Flex justify={"between"} align="center">
                    <Flex direction={"column"} align={"start"} gap={"2"}>
                      <Link href={`/issues/${issue.id}`}>{issue.title}</Link>
                      <IssueStatusBadge status={issue.status} />
                    </Flex>
                    <Avatar
                      size="3"
                      fallback={issue.assignedToUser?.email?.slice(0, 1) || "?"}
                      radius="full"
                      src={issue.assignedToUser?.image || "?"}
                    />
                  </Flex>
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table.Root>
    </Card>
  );
};

export default LatestIssues;

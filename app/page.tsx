import { Flex, Grid } from "@radix-ui/themes";
import IssueChart from "./IssueChart";
import IssueSummary from "./IssueSummary";
import LatestIssues from "./LatestIssues";
import prisma from "@/prisma/client";
import { Metadata } from "next";

export default async function Home() {
  const closedCount = await prisma.issue.count({
    where: {
      status: "CLOSED",
    },
  });
  const openCount = await prisma.issue.count({
    where: {
      status: "OPEN",
    },
  });
  const inProgressCount = await prisma.issue.count({
    where: {
      status: "IN_PROGRESS",
    },
  });

  const issueStatusProps = {
    openCount,
    closedCount,
    inProgressCount,
  };

  return (
    <Grid columns={{ initial: "1", md: "2" }} gap={"5"} p={"7"} pt={"8"}>
      <Flex direction={"column"} justify={"center"} gap={"7"}>
        <IssueSummary issueStatusProps={issueStatusProps} />
        <IssueChart issueStatusProps={issueStatusProps} />
      </Flex>
      <LatestIssues />
    </Grid>
  );
}

export const metadata: Metadata = {
  title: "Dashboard - Issue Tracker",
  description: "Summary of project issues",
};

import { default as NextLink } from "next/link";
import React from "react";
import { Link as RadixLink } from "@radix-ui/themes";

const Link = ({ children, href }: { children: any; href: string }) => {
  return (
    <NextLink href={href} legacyBehavior>
      <RadixLink highContrast underline="hover">
        {children}
      </RadixLink>
    </NextLink>
  );
};

export default Link;

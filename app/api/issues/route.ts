import { NextRequest } from "next/server";
import prisma from "@/prisma/client";
import { issueSchema as createIssueSchema } from "../../validationSchema";
import { nextAuthOptions } from "@/app/auth/authOptions";
import { getServerSession } from "next-auth";

export async function POST(request: NextRequest) {
  // Protecting Api routes handlers.
  const session = await getServerSession(nextAuthOptions);
  if (!session) {
    return Response.json({ error: "access denied" }, { status: 401 });
  }

  const body = await request.json();
  const isValid = createIssueSchema.safeParse(body);
  if (!isValid.success) {
    return Response.json("Inconsistant post data");
  }
  try {
    const issue = await prisma.issue.create({
      data: body,
    });
    return Response.json(issue, { status: 201 });
  } catch (error) {
    console.log(error);
    return Response.json(error, { status: 500 });
  }
}

"use client";

import { ErrorMessage } from "@/app/Components";
import { issueSchema as createIssueSchema } from "@/app/validationSchema";
import { zodResolver } from "@hookform/resolvers/zod";
import { Issue } from "@prisma/client";
import { Button, Callout, TextField } from "@radix-ui/themes";
import axios from "axios";
import "easymde/dist/easymde.min.css";
import dynamic from "next/dynamic";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { z } from "zod";

type IssueFormType = z.infer<typeof createIssueSchema>;

const SimpleMDE = dynamic(() => import("react-simplemde-editor"), {
  ssr: false,
});

const IssueForm = ({ issue }: { issue?: Issue }) => {
  const {
    register,
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<IssueFormType>({
    resolver: zodResolver(createIssueSchema),
  });
  const router = useRouter();

  const [error, setError] = useState<boolean | string>(false);

  const onSubmit = async (data: IssueFormType) => {
    console.log("submitted");
    try {
      if (issue) {
        const res = await axios.patch(
          `/api/issues/${issue.id}`,
          { ...data },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        if (res.status === 200) {
          router.push("/issues");
          router.refresh();
          console.log("Executed?");
        }
      } else {
        const res = await axios.post("/api/issues", data);
        if (res.status === 201) {
          router.push("/issues");
          router.refresh();
        } else {
          throw new Error("Something went Wrong!");
        }
      }
    } catch (error) {
      setError("Something went Wrong!");
      console.log(error);
    }
  };

  return (
    <div>
      {error && (
        <Callout.Root color="red" role="alert">
          <Callout.Text>{error}</Callout.Text>
        </Callout.Root>
      )}

      <form
        onSubmit={handleSubmit(onSubmit)}
        className="max-w-xl m-6 space-y-4"
      >
        <TextField.Input
          placeholder="Title"
          defaultValue={issue?.title}
          {...register("title")}
        />
        <ErrorMessage>{errors.title?.message}</ErrorMessage>
        <Controller
          control={control}
          name="description"
          render={({ field: { onChange, onBlur, value } }) => (
            <SimpleMDE
              placeholder="Description"
              onChange={onChange}
              onBlur={onBlur}
              value={issue?.description}
            />
          )}
        />
        <ErrorMessage>{errors.description?.message}</ErrorMessage>
        <Button className="hover:cursor-pointer" type="submit">
          {!issue ? "Submit New Issue" : "Update Issue"}
          {/* <Spinner /> */}
        </Button>
      </form>
    </div>
  );
};

export default IssueForm;

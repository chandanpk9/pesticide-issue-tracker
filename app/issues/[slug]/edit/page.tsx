import React from "react";
import IssueForm from "../../__components/IssueForm";
import prisma from "@/prisma/client";
import { notFound } from "next/navigation";

type RouteParam = {
  params: {
    slug: string;
  };
};

const page = async ({ params: { slug } }: RouteParam) => {
  const issue = await prisma.issue.findUnique({
    where: {
      id: parseInt(slug),
    },
  });

  if (!issue) notFound();

  return (
    <div>
      <IssueForm issue={issue} />
    </div>
  );
};

export default page;

"use client";

import {
  ChevronLeftIcon,
  ChevronRightIcon,
  DoubleArrowLeftIcon,
  DoubleArrowRightIcon,
} from "@radix-ui/react-icons";
import { Button, Flex, Text } from "@radix-ui/themes";
import { useRouter, useSearchParams } from "next/navigation";
import React from "react";

interface Props {
  itemCount: number;
  pageSize: number;
  currentPage: number;
}

const Pagination = ({ itemCount, pageSize, currentPage }: Props) => {
  const router = useRouter();
  const searchParams = useSearchParams();
  const handleClick = (val: number): void => {
    const params = new URLSearchParams(searchParams);
    params.set("page", String(val));
    router.push("/issues?" + params.toString());
  };

  const pageCount = Math.ceil(itemCount / pageSize);
  if (pageCount <= 1) return null;
  return (
    <Flex align={"center"} gap={"2"} m={"2"}>
      <Text>
        page {currentPage} of {pageCount}
      </Text>
      <Flex gap={"2"}>
        <Button variant="soft" color="indigo" onClick={() => handleClick(1)}>
          <DoubleArrowLeftIcon />
        </Button>
        <Button
          variant="soft"
          color="indigo"
          disabled={currentPage === 1}
          onClick={() => handleClick(currentPage - 1)}
        >
          <ChevronLeftIcon />
        </Button>
        <Button
          variant="soft"
          color="indigo"
          disabled={currentPage === pageCount}
          onClick={() => handleClick(currentPage + 1)}
        >
          <ChevronRightIcon />
        </Button>
        <Button
          variant="soft"
          color="indigo"
          onClick={() => handleClick(pageCount)}
        >
          <DoubleArrowRightIcon />
        </Button>
      </Flex>
    </Flex>
  );
};

export default Pagination;

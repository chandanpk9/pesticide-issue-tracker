"use client";

import {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
} from "recharts";
import { IssueCountProps } from "./IssueSummary";
import { Card } from "@radix-ui/themes";

const IssueChart = ({
  issueStatusProps: { openCount, inProgressCount, closedCount },
}: IssueCountProps) => {
  const data: { label: string; value: number }[] = [
    { label: "Open", value: openCount },
    { label: "In Progress", value: inProgressCount },
    { label: "Closed", value: closedCount },
  ];
  return (
    <Card>
      <ResponsiveContainer width={"100%"} height={300}>
        <BarChart width={600} height={300} data={data}>
          <XAxis dataKey="label" stroke="#8884d8" />
          <YAxis />
          <Tooltip />
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
          <Bar dataKey="value" fill="#8884d8" barSize={30} />
        </BarChart>
      </ResponsiveContainer>
    </Card>
  );
};

export default IssueChart;

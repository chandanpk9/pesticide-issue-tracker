import { Pencil2Icon } from "@radix-ui/react-icons";
import { Button } from "@radix-ui/themes";
import Link from "next/link";

const EditButton = ({ id }: { id: number }) => {
  return (
    <>
      <Link href={`/issues/${id}/edit`}>
        <Button className="hover:cursor-pointer">
          <Pencil2Icon height={"16"} width={"16"} />
          Edit Issue
        </Button>
      </Link>
    </>
  );
};

export default EditButton;

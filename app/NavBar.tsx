"use client";

import Link from "next/link";
import React from "react";
import { BsFillBugFill } from "react-icons/bs";
import { usePathname } from "next/navigation";
import { useSession } from "next-auth/react";
import { Avatar, Box, Button, Flex, HoverCard, Text } from "@radix-ui/themes";

interface NavLinks {
  path: string;
  label: string;
}

const NavBar = () => {
  return (
    <nav className="border-b-2 px-5 py-3">
      <Flex justify={"between"}>
        <NavLinks />
        <Box>
          <AuthStatus />
        </Box>
      </Flex>
    </nav>
  );
};

const NavLinks = () => {
  const currentPath = usePathname();

  const navLinks: NavLinks[] = [
    { path: "/", label: "Dashboard" },
    { path: "/issues", label: "Issues" },
  ];

  return (
    <Flex align={"center"} gap={"3"}>
      <Link href="/">
        <BsFillBugFill />
      </Link>
      <ul className="flex space-x-6">
        {navLinks.map((item) => (
          <li
            className={`${
              currentPath === item.path
                ? "text-zinc-900 font-bold"
                : "text-zinc-700"
            }`}
            key={item.path}
          >
            <Link href={item.path}>{item.label}</Link>
          </li>
        ))}
      </ul>
    </Flex>
  );
};

const AuthStatus = () => {
  const { status, data: session } = useSession();
  if (status === "loading") return null;

  return (
    <>
      {status === "authenticated" ? (
        <HoverCard.Root>
          <HoverCard.Trigger>
            <Avatar
              size="3"
              fallback="c"
              radius="full"
              src={session?.user?.image || "C"}
            />
          </HoverCard.Trigger>
          <HoverCard.Content size={"2"} className="space-y-3">
            <Text>{session?.user?.email}</Text>
            <Box>
              <Button size={"3"} className="w-full">
                {status === "authenticated" && (
                  <Link href={"/api/auth/signout"}>Log out</Link>
                )}
              </Button>
            </Box>
          </HoverCard.Content>
        </HoverCard.Root>
      ) : (
        <Button size={"3"}>
          <Link href={"/api/auth/signin"}>Sign In</Link>
        </Button>
      )}
    </>
  );
};

export default NavBar;

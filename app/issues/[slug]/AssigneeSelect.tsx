"use client";
import { Issue, User } from "@prisma/client";
import { Select } from "@radix-ui/themes";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";

const AssigneeSelect = ({ issue }: { issue: Issue }) => {
  const {
    data: users,
    isLoading,
    error,
  } = useQuery<User[]>({
    queryKey: ["users"],
    queryFn: () => axios.get("/api/users").then((res) => res.data.users),
    staleTime: 60 * 1000,
    retry: 3,
  });

  return (
    <Select.Root
      defaultValue={issue.assignedToUserId || "unassigned"}
      size={"2"}
      onValueChange={(userId) =>
        axios.patch("/api/issues/" + issue.id, {
          assignedToUserId: userId === "unassigned" ? null : userId,
        })
      }
    >
      <Select.Trigger placeholder="Assigne..." />
      <Select.Content>
        <Select.Group>
          <Select.Label>Sugggestions</Select.Label>
          <Select.Item value={"unassigned"}>Unassigned</Select.Item>
          <Select.Separator />
          {users?.map((user) => (
            <Select.Item key={user.id} value={user.id}>
              {user.email}
            </Select.Item>
          ))}
        </Select.Group>
      </Select.Content>
    </Select.Root>
  );
};

export default AssigneeSelect;

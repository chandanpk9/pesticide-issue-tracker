import { Status } from "@prisma/client";
import { Card, Flex, Text } from "@radix-ui/themes";
import Link from "next/link";
import React from "react";

export interface IssueCountProps {
  issueStatusProps: {
    openCount: number;
    closedCount: number;
    inProgressCount: number;
  };
}

const IssueSummary = ({
  issueStatusProps: { openCount, inProgressCount, closedCount },
}: IssueCountProps) => {
  const issueStatuses: {
    label: string;
    value: number;
    status: Status;
  }[] = [
    {
      label: "Open Issues",
      value: openCount,
      status: "OPEN",
    },
    {
      label: "In_Progress Issues",
      value: inProgressCount,
      status: "IN_PROGRESS",
    },
    { label: "Closed Issues", value: closedCount, status: "CLOSED" },
  ];

  return (
    <Flex gap={"5"}>
      {issueStatuses.map((item, i) => {
        return (
          <Card key={i}>
            <Link href={`/issues?status=${item.status}`}>
              <Flex direction={"column"} gap={"1"}>
                <Text className="text-sm">{item.label}</Text>
                <Text className="font-bold text-xl">{item.value}</Text>
              </Flex>
            </Link>
          </Card>
        );
      })}
    </Flex>
  );
};

export default IssueSummary;

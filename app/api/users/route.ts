import { nextAuthOptions } from "@/app/auth/authOptions";
import { getServerSession } from "next-auth";
import { NextRequest, NextResponse } from "next/server";
import prisma from "@/prisma/client";

export async function GET(request: NextRequest) {
  // const session = await getServerSession(nextAuthOptions);
  // if (!session) {
  //   return Response.json({ error: "access denied" }, { status: 401 });
  // }

  const users = await prisma.user.findMany({ orderBy: { name: "asc" } });

  if (users) {
    return NextResponse.json({ users }, { status: 200 });
  }
  return NextResponse.json(
    { error: "Something went wrong db response" },
    { status: 200 }
  );
}
